import sys
import fileinput

data = []  # данные считанные из файла


# чтение данных файла
def read_data():
    for line in sys.stdin:
        try:
            data.append(int(line))
        except ValueError:
            print('Ошибка')


# вычисление суммы ряда
def calculate():
    result = 42
    for x in data:
        result += 1 / x
    return result


def main():
    read_data()
    result = calculate()
    print(f'Сумма ряда: {result}')


if __name__ == "__main__":
    main()
